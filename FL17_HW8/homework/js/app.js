'use strict';

let numberThree = 3;
let game = function game() {
 let playerScore = 0;
 let computerScore = 0;
 let moves = 0;
 let playerChoice = '';
 let computerChoice = '';
 let playGame = function playGame() {
 let rockBtn = document.querySelector('.rock');
 let paperBtn = document.querySelector('.paper');
 let scissorBtn = document.querySelector('.scissor');
 let playerOptions = [rockBtn, paperBtn, scissorBtn];
 let computerOptions = ['rock', 'paper', 'scissors'];

 playerOptions.forEach(function (option) {
 option.addEventListener('click', function () {
 moves++;

 let choiceNumber = Math.floor(Math.random() * numberThree);
 let computerChoice = computerOptions[choiceNumber];

 winner(this.innerText, computerChoice);

 if (playerScore === numberThree || computerScore === numberThree) {
  gameOver(playerOptions);
 }
});
  });
 };

 let winner = function winner(player, computer) {
  let result = document.querySelector('.result');
  let playerScoreBoard = document.querySelector('.p-count');
  let computerScoreBoard = document.querySelector('.c-count');
  player = player.toLowerCase();
  computer = computer.toLowerCase();
  playerChoice = player;
  computerChoice = computer;
  if (player === computer) {
result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', DRAW';
  } else if (player === 'rock') {
if (computer === 'paper') {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve LOST!';
 computerScore++;
 computerScoreBoard.textContent = computerScore;
} else {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve WON!';
 playerScore++;
 playerScoreBoard.textContent = playerScore;
}
  } else if (player === 'scissors') {
if (computer === 'rock') {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve LOST!';
 computerScore++;
 computerScoreBoard.textContent = computerScore;
} else {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve WON!';
 playerScore++;
 playerScoreBoard.textContent = playerScore;
}
  } else if (player === 'paper') {
if (computer === 'scissors') {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve LOST!';
 computerScore++;
 computerScoreBoard.textContent = computerScore;
} else {
 result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', You\u2019ve WON!';
 playerScore++;
 playerScoreBoard.textContent = playerScore;
}
  }
 };

 let gameOver = function gameOver(playerOptions) {
 let chooseMove = document.querySelector('.move');
 let result = document.querySelector('.result');

playerOptions.forEach(function (option) {
option.style.display = 'none';
  });

  chooseMove.innerText = 'Game Over!!';

  if (playerScore > computerScore) {
result.style.fontSize = '2rem';
result.innerText = 'You Won The Game';
result.style.color = '#00fff3';
  } else if (playerScore < computerScore) {
result.style.fontSize = '2rem';
result.innerText = 'You Lost The Game';
result.style.color = 'red';
  } else {
result.style.fontSize = '2rem';
result.textContent = 'Round ' + moves + ', ' + playerChoice + ' vs. ' + computerChoice + ', DRAW';
result.style.color = 'grey';
  }
 };
 playGame();
};

game();