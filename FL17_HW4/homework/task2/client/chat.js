let socket = new WebSocket('ws://localhost:8080');
socket.onopen = function() {
socket.onmessage = function(event) {
    let message = event.data;

  
    let messageElem = document.createElement('div');
    messageElem.setAttribute('id', 'received_message');
    messageElem.innerHTML = message;
    document.getElementById('div_messages_container').append(messageElem);
};
};

let username=prompt('What is your name?', 'Anonymous');

function sendMessage(){
    let message=document.getElementById('message_textarea').value;
    let date=new Date();
    let time=date.toLocaleString('en-US', { hour: 'numeric', hour12: true ,minute: 'numeric', second:'numeric' });
    socket.send('<i>'+username+'</i><br><div id='+'received_msg_text'+'>'+message+'</div>'+time);

    document.getElementById('message_textarea').value='';

    let myMessage = document.createElement('div');
    myMessage.setAttribute('id', 'my_message');
    myMessage.innerHTML = '<i>'+username+'</i><br><div id='+'sent_msg_text'+'>'+message+'</div><br><br>'+time;
    document.getElementById('div_messages_container').append(myMessage);
    
}

document.getElementById('message_textarea').addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
        sendMessage();
    }
});



