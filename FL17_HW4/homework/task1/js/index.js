
/*jshint esversion: 6 */
let input_temp='';
let id='1';
let url='https://jsonplaceholder.typicode.com/users/'+id;
let updateOrDeleteSelector=0;

const four=4;
const twohundred=200;
const fivehundred=500;

let div_loading = document.createElement('div');
div_loading.style = 'position:relative;width:500px';
div_loading.setAttribute('id','div_loading');

document.body.append(div_loading);

let div_wrapper = document.createElement('div');
div_wrapper.setAttribute('id','div_wrapper');
document.body.append(div_wrapper);

let p_id_input_text = document.createElement('p');
p_id_input_text.setAttribute('id','p_id_input_text');
p_id_input_text.innerHTML='Please select ID:';
div_wrapper.append(p_id_input_text);

let id_input = document.createElement('input');
id_input.setAttribute('id','id_input');
id_input.value=id;
div_wrapper.append(id_input);


function buildEditableForm(id){
  let keyDivStyle='float:left;position:relative;width:80px;margin:17px 20px 17px 20px';
  let valueDivStyle='float:left;position:relative;width:400px;margin-top:17px';
  let hrDivStyle='float:left;position:relative;width:100%;margin:0';
  let hrDivStyleDotted='float:left;position:relative;width:100%;margin:0;border-top:1px dotted white';


    let div_cont = document.createElement('div');
    div_cont.style = 'border:1px solid black;position:relative;width:600px;min-height:1050px';
    div_cont.setAttribute('id','div_container'+id);

    let div_id_key = document.createElement('div');
    div_id_key.style = keyDivStyle;
    div_id_key.setAttribute('id','div_id_key'+id);

    let div_id_value = document.createElement('div');
    div_id_value.style = valueDivStyle;
    div_id_value.setAttribute('contenteditable','true');
    div_id_value.setAttribute('id','div_id_value'+id);

    let hr0 = document.createElement('hr');
    hr0.style = hrDivStyle;


    let div_name_key = document.createElement('div');
    div_name_key.style = keyDivStyle;
    div_name_key.setAttribute('id','div_name_key'+id);

    let div_name_value = document.createElement('div');
    div_name_value.style = valueDivStyle;
    div_name_value.setAttribute('contenteditable','true');
    div_name_value.setAttribute('id','div_name_value'+id);

    let hr1 = document.createElement('hr');
    hr1.style = hrDivStyle;


    let div_username_key = document.createElement('div');
    div_username_key.style = keyDivStyle;
    div_username_key.setAttribute('id','div_username_key'+id);

    let div_username_value = document.createElement('div');
    div_username_value.style = valueDivStyle;
    div_username_value.setAttribute('contenteditable','true');
    div_username_value.setAttribute('id','div_username_value'+id);

    let hr2 = document.createElement('hr');
    hr2.style = hrDivStyle;
    
    
    let div_email_key = document.createElement('div');
    div_email_key.style = keyDivStyle;
    div_email_key.setAttribute('id','div_email_key'+id);

    let div_email_value = document.createElement('div');
    div_email_value.style = valueDivStyle;
    div_email_value.setAttribute('contenteditable','true');
    div_email_value.setAttribute('id','div_email_value'+id);

    let hr3 = document.createElement('hr');
    hr3.style = hrDivStyle;


    let div_address_key = document.createElement('div');
    div_address_key.style = keyDivStyle;
    div_address_key.setAttribute('id','div_address_key'+id);

      let div_address_street_key = document.createElement('div');
      div_address_street_key.style = keyDivStyle;
      div_address_street_key.setAttribute('id','div_address_street_key'+id);

      let div_address_street_value = document.createElement('div');
      div_address_street_value.style = valueDivStyle;
      div_address_street_value.setAttribute('contenteditable','true');
      div_address_street_value.setAttribute('id','div_address_street_value'+id);
      
      let hr4 = document.createElement('hr');
      hr4.style = hrDivStyleDotted;

      let div_address_suite_key = document.createElement('div');
      div_address_suite_key.style = keyDivStyle;
      div_address_suite_key.setAttribute('id','div_address_suite_key'+id);

      let div_address_suite_value = document.createElement('div');
      div_address_suite_value.style = valueDivStyle;
      div_address_suite_value.setAttribute('contenteditable','true');
      div_address_suite_value.setAttribute('id','div_address_suite_value'+id);
      
      let hr5 = document.createElement('hr');
      hr5.style = hrDivStyleDotted;

      let div_address_city_key = document.createElement('div');
      div_address_city_key.style = keyDivStyle;
      div_address_city_key.setAttribute('id','div_address_city_key'+id);

      let div_address_city_value = document.createElement('div');
      div_address_city_value.style = valueDivStyle;
      div_address_city_value.setAttribute('contenteditable','true');
      div_address_city_value.setAttribute('id','div_address_city_value'+id);
      
      let hr6 = document.createElement('hr');
      hr6.style = hrDivStyleDotted;

      let div_address_zipcode_key = document.createElement('div');
      div_address_zipcode_key.style = keyDivStyle;
      div_address_zipcode_key.setAttribute('id','div_address_zipcode_key'+id);

      let div_address_zipcode_value = document.createElement('div');
      div_address_zipcode_value.style = valueDivStyle;
      div_address_zipcode_value.setAttribute('contenteditable','true');
      div_address_zipcode_value.setAttribute('id','div_address_zipcode_value'+id);
      
      let hr7 = document.createElement('hr');
      hr7.style = hrDivStyleDotted;

        let div_address_geo_key = document.createElement('div');
        div_address_geo_key.style = keyDivStyle;
        div_address_geo_key.setAttribute('id','div_address_geo_key'+id);

        let div_address_geo_lat_key = document.createElement('div');
        div_address_geo_lat_key.style = keyDivStyle;
        div_address_geo_lat_key.setAttribute('id','div_address_geo_lat_key'+id);

        let div_address_geo_lat_value = document.createElement('div');
        div_address_geo_lat_value.style = valueDivStyle;
        div_address_geo_lat_value.setAttribute('contenteditable','true');
        div_address_geo_lat_value.setAttribute('id','div_address_geo_lat_value'+id);

        let div_address_geo_lng_key = document.createElement('div');
        div_address_geo_lng_key.style = keyDivStyle;
        div_address_geo_lng_key.setAttribute('id','div_address_geo_lng_key'+id);

        let div_address_geo_lng_value = document.createElement('div');
        div_address_geo_lng_value.style = valueDivStyle;
        div_address_geo_lng_value.setAttribute('contenteditable','true');
        div_address_geo_lng_value.setAttribute('id','div_address_geo_lng_value'+id);
      
    let hr8 = document.createElement('hr');
    hr8.style = hrDivStyleDotted;

    let div_phone_key = document.createElement('div');
    div_phone_key.style = keyDivStyle;
    div_phone_key.setAttribute('id','div_phone_key'+id);

    let div_phone_value = document.createElement('div');
    div_phone_value.style = valueDivStyle;
    div_phone_value.setAttribute('contenteditable','true');
    div_phone_value.setAttribute('id','div_phone_value'+id);
    
    let hr9 = document.createElement('hr');
    hr9.style = hrDivStyle;

    let div_website_key = document.createElement('div');
    div_website_key.style = keyDivStyle;
    div_website_key.setAttribute('id','div_website_key'+id);

    let div_website_value = document.createElement('div');
    div_website_value.style = valueDivStyle;
    div_website_value.setAttribute('contenteditable','true');
    div_website_value.setAttribute('id','div_website_value'+id);
    
    let hr10 = document.createElement('hr');
    hr10.style = hrDivStyle;

    let div_company_key = document.createElement('div');
    div_company_key.style = keyDivStyle;
    div_company_key.setAttribute('id','div_company_key'+id);

      let div_company_name_key = document.createElement('div');
      div_company_name_key.style = keyDivStyle;
      div_company_name_key.setAttribute('id','div_company_name_key'+id);

      let div_company_name_value = document.createElement('div');
      div_company_name_value.style = valueDivStyle;
      div_company_name_value.setAttribute('contenteditable','true');
      div_company_name_value.setAttribute('id','div_company_name_value'+id);

      let hr11 = document.createElement('hr');
      hr11.style = hrDivStyle;

      let div_company_catchphrase_key = document.createElement('div');
      div_company_catchphrase_key.style = keyDivStyle;
      div_company_catchphrase_key.setAttribute('id','div_company_catchphrase_key'+id);

      let div_company_catchphrase_value = document.createElement('div');
      div_company_catchphrase_value.style = valueDivStyle;
      div_company_catchphrase_value.setAttribute('contenteditable','true');
      div_company_catchphrase_value.setAttribute('id','div_company_catchphrase_value'+id);
    
      let hr12 = document.createElement('hr');
      hr12.style = hrDivStyleDotted;

      let div_company_bs_key = document.createElement('div');
      div_company_bs_key.style = keyDivStyle;
      div_company_bs_key.setAttribute('id','div_company_bs_key'+id);

      let div_company_bs_value = document.createElement('div');
      div_company_bs_value.style = valueDivStyle;
      div_company_bs_value.setAttribute('contenteditable','true');
      div_company_bs_value.setAttribute('id','div_company_bs_value'+id);
    
  let hr13 = document.createElement('hr');
  hr13.style = hrDivStyleDotted;
  let hr14 = document.createElement('hr');
  hr14.style = hrDivStyle;
  let hr15 = document.createElement('hr');
  hr15.style = hrDivStyleDotted;
  let hr16 = document.createElement('hr');
  hr16.style = hrDivStyleDotted;
  let hr17 = document.createElement('hr');
  hr17.style = hrDivStyleDotted;
  let hr18 = document.createElement('hr');
  hr18.style = hrDivStyle;


  let button_update = document.createElement('button');
  button_update.innerHTML = 'UPDATE';
  button_update.style = 'float:left; width:100px; height:30px; margin: 20px';
  button_update.setAttribute('id','button_update'+id);
  button_update.setAttribute('onClick','putUser('+id+')');

  let button_delete = document.createElement('button');
  button_delete.innerHTML = 'DELETE';
  button_delete.style = 'float:left; width:100px; height:30px; margin: 20px';
  button_delete.setAttribute('id','button_delete'+id);
  button_delete.setAttribute('onClick','deleteUser('+id+')');

  
    
    div_wrapper.append(div_cont);
      div_cont.append(div_id_key);
      div_cont.append(div_id_value);
      div_cont.append(hr0);

      div_cont.append(div_name_key);
      div_cont.append(div_name_value);
      div_cont.append(hr1);

      div_cont.append(div_username_key);
      div_cont.append(div_username_value);
      div_cont.append(hr2);

      div_cont.append(div_email_key);
      div_cont.append(div_email_value);
      div_cont.append(hr3);

      div_cont.append(div_address_key);

        div_cont.append(hr15);

        div_cont.append(div_address_street_key);
        div_cont.append(div_address_street_value);
        div_cont.append(hr4);

        div_cont.append(div_address_suite_key);
        div_cont.append(div_address_suite_value);
        div_cont.append(hr5);

        div_cont.append(div_address_city_key);
        div_cont.append(div_address_city_value);
        div_cont.append(hr6);

        div_cont.append(div_address_zipcode_key);
        div_cont.append(div_address_zipcode_value);
        div_cont.append(hr7);

        div_cont.append(div_address_geo_key);

        div_cont.append(hr16);

          div_cont.append(div_address_geo_lat_key);
          div_cont.append(div_address_geo_lat_value);
          div_cont.append(hr8);

          div_cont.append(div_address_geo_lng_key);
          div_cont.append(div_address_geo_lng_value);
          div_cont.append(hr9);

      div_cont.append(div_phone_key);
      div_cont.append(div_phone_value);
      div_cont.append(hr10);

      div_cont.append(div_website_key);
      div_cont.append(div_website_value);
      div_cont.append(hr11);

      div_cont.append(div_company_key);

      div_cont.append(hr17);

        div_cont.append(div_company_name_key);
        div_cont.append(div_company_name_value);
        div_cont.append(hr12);

        div_cont.append(div_company_catchphrase_key);
        div_cont.append(div_company_catchphrase_value);
        div_cont.append(hr13);

        div_cont.append(div_company_bs_key);
        div_cont.append(div_company_bs_value);

    div_cont.append(hr18);
    div_cont.append(button_update);
    div_cont.append(button_delete);
}

function putInfoInsideForm(idf){
  document.getElementById('div_id_key'+idf).innerHTML='Id';
          document.getElementById('div_id_value'+idf).innerHTML+=input_temp.id;
          document.getElementById('div_name_key'+idf).innerHTML='Name';
          document.getElementById('div_name_value'+idf).innerHTML+=input_temp.name;
          document.getElementById('div_username_key'+idf).innerHTML='Username';
          document.getElementById('div_username_value'+idf).innerHTML+=input_temp.username;
          document.getElementById('div_email_key'+idf).innerHTML='Email';
          document.getElementById('div_email_value'+idf).innerHTML+=input_temp.email;
          document.getElementById('div_address_key'+idf).innerHTML='Address';


            document.getElementById('div_address_street_key'+idf).innerHTML='Street';
            document.getElementById('div_address_street_value'+idf).innerHTML+=input_temp.address.street;
            document.getElementById('div_address_suite_key'+idf).innerHTML='Suite';
            document.getElementById('div_address_suite_value'+idf).innerHTML+=input_temp.address.suite;
            document.getElementById('div_address_city_key'+idf).innerHTML='City';
            document.getElementById('div_address_city_value'+idf).innerHTML+=input_temp.address.city;
            document.getElementById('div_address_zipcode_key'+idf).innerHTML='Zipcode';
            document.getElementById('div_address_zipcode_value'+idf).innerHTML+=input_temp.address.zipcode;
            document.getElementById('div_address_geo_key'+idf).innerHTML='Geo';
              document.getElementById('div_address_geo_lat_key'+idf).innerHTML='Lat';
              document.getElementById('div_address_geo_lat_value'+idf).innerHTML+=input_temp.address.geo.lat;
              document.getElementById('div_address_geo_lng_key'+idf).innerHTML='Lng';
              document.getElementById('div_address_geo_lng_value'+idf).innerHTML+=input_temp.address.geo.lng;
          document.getElementById('div_phone_key'+idf).innerHTML='Phone';
          document.getElementById('div_phone_value'+idf).innerHTML+=input_temp.phone;
          document.getElementById('div_website_key'+idf).innerHTML='Website';
          document.getElementById('div_website_value'+idf).innerHTML+=input_temp.website;
          document.getElementById('div_company_key'+idf).innerHTML='Company';
            document.getElementById('div_company_name_key'+idf).innerHTML='Name';
            document.getElementById('div_company_name_value'+idf).innerHTML+=input_temp.company.name;
            document.getElementById('div_company_catchphrase_key'+idf).innerHTML='Catchphrase';
            document.getElementById('div_company_catchphrase_value'+idf).innerHTML+=input_temp.company.catchPhrase;
            document.getElementById('div_company_bs_key'+idf).innerHTML='Bs';
            document.getElementById('div_company_bs_value'+idf).innerHTML+=input_temp.company.bs;
}

const xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
  if (this.readyState === four && this.status === twohundred) {


    setTimeout(function(){
      input_temp = xhr.response;
      document.getElementById('div_loading').innerHTML = '';
          buildEditableForm(1);
          putInfoInsideForm(1);
    }, fivehundred);
  } else {
    document.getElementById('div_loading').innerHTML = `<img 
    src='https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif' 
    alt='Loading'>`;
  }
};

xhr.open('GET', url);

xhr.responseType = 'json';
  xhr.send();

function changeUserId(){

  document.getElementById('div_container'+id).remove();

  let userID = document.getElementById('id_input').value;
  id=userID;


url='https://jsonplaceholder.typicode.com/users/'+userID;
xhr.onreadystatechange = function() {
  if (this.readyState === four && this.status === twohundred) {
    setTimeout(function(){
      input_temp = xhr.response;
      document.getElementById('div_loading').innerHTML = '';
          buildEditableForm(userID);
          putInfoInsideForm(userID);
    }, fivehundred);
  } else {
    document.getElementById('div_loading').innerHTML = `<img 
    src='https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif' 
    alt='Loading'>`;
  }
};
  xhr.open('GET', url);
  xhr.responseType = 'json';
    xhr.send();
  }


  function putUser(userID){
    if(updateOrDeleteSelector===1){
      let webURL='https://jsonplaceholder.typicode.com/users/'+userID;
      let post_data=JSON.stringify(document.getElementById('div_container'+userID).innerHTML);
      xhr.open('PUT', webURL);
      xhr.send(post_data);
      }
    }

  function deleteUser(userID){
    if(updateOrDeleteSelector===1){
      let webURL='https://jsonplaceholder.typicode.com/users/'+userID;
      xhr.open('DELETE', webURL);
      xhr.send();
      }
    }

    putUser(1);
    deleteUser(1);

  id_input.addEventListener('input', changeUserId);