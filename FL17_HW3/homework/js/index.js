/*jshint esversion: 6 */
(function () {
    'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
class Pizza { 
    constructor(size, type) {
        this.size = size;
        this.type = type;
    }

    addExtraIngredient(newIngredient) {

        // Unusual arguments quantity
        if (this.size===undefined && this.type!==undefined){
            throw new PizzaException('Required two arguments, given: 1');
        }
        if (this.size!==undefined && this.type===undefined){
            throw new PizzaException('Required two arguments, given: 1');
        }
        if (this.size===undefined && this.type===undefined){
            throw new PizzaException('Required two arguments, given: 0');
        }

        //Invalid size
        if (!Pizza.allowedSizes.includes(this.size)) {
            throw new PizzaException('Invalid size');
        }

        //Invalid type
        if (!Pizza.allowedTypes.includes(this.type)) {
            throw new PizzaException('Invalid type');
        }

        //Invalid ingredient
        if (!Pizza.allowedExtraIngredients.includes(newIngredient)) {
            throw new PizzaException('Invalid ingredient');
        }

        //duplicate
        if (newIngredient.name === 'TOMATOES' && extraTomatoesCounter===1){
            throw new PizzaException('Duplicate ingredient');
        }

        if (newIngredient.name === 'CHEESE' && extraCheeseCounter===1){
            throw new PizzaException('Duplicate ingredient');
        }

        if (newIngredient.name === 'MEAT' && extraMeatCounter===1){
            throw new PizzaException('Duplicate ingredient');
        }



        //success
        if (newIngredient.name === 'TOMATOES' && extraTomatoesCounter===0){
            extraTomatoesCounter=1;
        }

        if (newIngredient.name === 'CHEESE' && extraCheeseCounter===0){
            extraCheeseCounter=1;
        }

        if (newIngredient.name === 'MEAT' && extraMeatCounter===0){
            extraMeatCounter=1;
        }
    }

    removeExtraIngredient(newIngredient){
        if (newIngredient.name === 'TOMATOES' && extraTomatoesCounter===1){
            extraTomatoesCounter=0;
        }

        if (newIngredient.name === 'CHEESE' && extraCheeseCounter===1){
            extraCheeseCounter=0;
        }

        if (newIngredient.name === 'MEAT' && extraMeatCounter===1){
            extraMeatCounter=0;
        }
    }

    getPrice(){
     let total = pizza.size.price + pizza.type.price;
        if (extraTomatoesCounter===1){
            total+=Pizza.EXTRA_TOMATOES.price;
        }
        if (extraCheeseCounter===1){
            total+=Pizza.EXTRA_CHEESE.price;
        }
        if (extraMeatCounter===1){
            total+=Pizza.EXTRA_MEAT.price;
        }
    return total;        
    }

    getSize(){
        let pizzaSize = pizza.size;
        return pizzaSize;
    }

    getExtraIngredients(){
        let extraIngredients=[];
        if (extraTomatoesCounter===1){
            extraIngredients.push('TOMATOES');
        }
        if (extraCheeseCounter===1){
            extraIngredients.push('CHEESE');
        }
        if (extraMeatCounter===1){
            extraIngredients.push('MEAT');
        }
        return extraIngredients;
    }

    getPizzaInfo(){
        let pizzaInfo = `Size: ${pizza.size.name}, type: ${pizza.type.name}; `;
        pizzaInfo += `extra ingredients: ${pizza.getExtraIngredients()}; price: ${pizza.getPrice()} UAH.`;
        return pizzaInfo;
    }
 }
 
/* Sizes, types and extra ingredients */
Pizza.SIZE_S = {
    name:'SMALL',
    price: 50
};
Pizza.SIZE_M = {
    name:'MEDIUM',
    price: 75
};
Pizza.SIZE_L = {
    name:'LARGE',
    price: 100
};

Pizza.TYPE_VEGGIE = {
    name:'VEGGIE',
    price: 50
};
Pizza.TYPE_MARGHERITA = {
    name:'MARGHERITA',
    price: 60
};
Pizza.TYPE_PEPPERONI = {
    name:'PEPPERONI',
    price: 70
};

let extraTomatoesCounter=0;
let extraCheeseCounter=0;
let extraMeatCounter=0;

Pizza.EXTRA_TOMATOES = {
    name:'TOMATOES',
    price: 5
};
Pizza.EXTRA_CHEESE = {
    name:'CHEESE',
    price: 7
};
Pizza.EXTRA_MEAT = {
    name:'MEAT',
    price: 9
};

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
class PizzaException { 
    constructor(errorMessage) {
        this.log = errorMessage;
    }
 }

/* It should work */ 

  // small pizza, type: veggie
 let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
  // add extra meat
 pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
  // check price
 console.log(`Price: ${pizza.getPrice()} UAH`); // => Price: 109 UAH
  // add extra corn
 pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
  // add extra corn
 pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
  // check price
 console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); //  Price: 121 UAH
  // check pizza size
 console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); // => Is pizza large: false
  // remove extra ingredient
 pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
 console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); // => Extra ingredients: 2
 console.log(pizza.getPizzaInfo()); // => Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.


}());