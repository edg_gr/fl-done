/*jshint esversion: 6 */

const numberOne = 1;
const numberTwo = 2;
const numberThree = 3;
const numberFive = 5;
const numberSixtyThousand = 60000;


class Magazine {
    constructor() {
        this.states = [
            'ReadyForPushNotification',
            'ReadyForApprove',
            'ReadyForPublish',
            'PublishInProgress'
        ];
        this.articles = [];
        this.followers = [];
        this.staff = [];

        this.current = this.states[0];
    }
}

class MagazineEmployee {
    constructor(name, role, magazine) {
        this.name = name;
        this.role = role;
        this.magazine = magazine;
        this.magazine.staff.push(this);
    }

    addArticle(article) {
        this.article = article;
        if (this.role !== 'manager') {
            this.magazine.articles.push(article);
        }
        if (this.magazine.articles.length >= numberFive) {
            this.magazine.current = this.magazine.states[1];
        }
    }

    approve() {
        if (this.magazine.articles.length < numberFive && this.role === 'manager') {
            console.log(`Hello ${this.name}. You can't approve. We don't 
         have enough of publications`);

        } else if (
            this.magazine.current === 'ReadyForApprove' &&
            this.role === 'manager' 
        ) {
            console.log(`Hello ${this.name}. You've approved the 
         changes`);
         this.magazine.current = this.magazine.states[numberTwo];

        } else if (
            this.magazine.current === 'ReadyForPublish' &&
            this.role === 'manager'
        ) {
            console.log(`Hello ${this.name}. Publications have already 
         been approved by you.`);

        } else if (this.magazine.current === 'PublishInProgress') {
            console.log(`Hello ${this.name}. While we are publishing we 
         can't do any actions`);
         
        } else {
            console.log('You do not have permissions to do it');
        }
    }

    publish() {
        if (this.magazine.current === 'ReadyForPublish') {
            console.log(`Hello ${this.name}. You've recently published 
        publications.`);
            this.magazine.current = this.magazine.states[numberThree];
            for (let value of this.magazine.articles) {
                let textOfArticle = value;
                console.log(textOfArticle);
                value += 1;
              }

            setTimeout(() => {
                this.magazine.current = this.magazine.states[0];
                this.magazine.articles = [];
                console.log(this.magazine.articles);
            }, numberSixtyThousand);
        } else if (this.magazine.current === 'PublishInProgress') {
            console.log(`Hello ${this.name}. While we are publishing we 
        can't do any actions`);
        } else if (this.magazine.current === 'ReadyForPushNotification') {
            console.log(`Hello ${this.name}. You can't publish. We are 
            creating publications now.`);
        } 
    }
}

class Follower {
    constructor(name) {
        this.name = name;
    }

    subscribeTo(magazine, topic) {
        this.magazine = magazine;
        this.topic = topic;
        this.magazine.followers.push(this);
    }

    unsubscribe() {
        const index = this.magazine.followers.indexOf(this);
        if (index !== -numberOne) {
            this.magazine.followers.splice(index, 1);
        }
    }
}


const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine); 
const general = new MagazineEmployee('Olha', 'general', magazine); 

const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');


iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');

sport.addArticle('something about sport');
politics.addArticle('something about politics');
general.addArticle('some general information');
politics.addArticle('something about politics again');
sport.approve();
manager.approve();
politics.publish();
sport.addArticle('news about sport');
manager.approve();
sport.publish();
manager.approve('news about sport'); 
