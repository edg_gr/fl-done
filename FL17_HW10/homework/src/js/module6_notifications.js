function showAlertMessage(message){
    alertMessage.innerHTML=message;
    alertMessage.classList.remove('hidden');
    setTimeout(hideAlertMessage, twothousand);
}

function hideAlertMessage(){
    alertMessage.classList.add('hidden');
}