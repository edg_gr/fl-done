/*jshint esversion: 9 */


const numberTwo=2;
const numberThree=3;
const numberFour=4;
const numberSix=6;
const numberTen=10;


function getMaxEvenElement(array) {
    return Math.max.apply(null, array);
}
function getMaxEvenElementReduce(arr) {
    const max = arr.reduce((a,b) => Math.max(a,b), -Infinity);
    return max;
}
function getMaxEvenElementThreeDots(arr) {
    let max = Math.max(...arr);
    return max;
}
const arr=['1', '3', '4', '2', '5'];
console.log(getMaxEvenElement(arr));
console.log(getMaxEvenElementReduce(arr));
console.log(getMaxEvenElementThreeDots(arr));


/*
let a = 3;
let b = 5;
[a, b] = [b, a];
console.log(a);
console.log(b);

function getValue(value){
let result=value ?? undefined;
return result;
}

console.log(getValue(0));
*/


function getObjFromArray(arrayOfArrays) {
    let key_one = Object.values(arrayOfArrays[0].slice(0,1));
    let value_one = Object.values(arrayOfArrays[0].slice(1));
    let key_two = Object.values(arrayOfArrays[1].slice(0,1));
    let value_two = Object.values(arrayOfArrays[1].slice(1));
    let key_three = Object.values(arrayOfArrays[numberTwo].slice(0,1));
    let value_three = Object.values(arrayOfArrays[numberTwo].slice(1));
    let result = key_one + ': "' + value_one + '", ' + key_two + 
    ': "' + value_two + '", ' + key_three + ': "' + value_three + '"' ;
    return result;
      
}
const arrayOfArrays = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'lviv']
];
console.log(getObjFromArray(arrayOfArrays));




function addUniqueID(obj){
    obj = {...obj, ...list[0]};
    return obj;
}

const list = [
    {id: Symbol()},
    {id: Symbol()}
];

const obj1 = {name:'nick'};

console.log(addUniqueID(obj1));
console.log(addUniqueID({name:'buffy'}));

console.log(Object.keys(obj1).includes('id'));




function getRegroupedObject(oldObj){
    const{
        name: firstName,
        details: { id, age, university }
    } = oldObj;
    const user = {age, firstName, id};
    return {university, user};
}

const oldObj = {
    name: 'willow',
    details: { id: 1, age: 47, university: 'LNU'}
};

console.log(getRegroupedObject(oldObj));



function getArrayWithUniqueElements(arr){
    let mySet = [...new Set(arr)];
    return mySet;
}

const arra = [numberTwo, numberThree, numberFour, numberTwo, numberFour, 'a', 'c', 'a'];
console.log(getArrayWithUniqueElements(arra));



function hideNumber(number){
    let cutNumber=number.substring(numberSix);
    let numberWithAsterisks=cutNumber.padStart(numberTen,'*');
    return numberWithAsterisks;
}

const phoneNumber = '0123456789';
console.log(hideNumber(phoneNumber));



function add(a=required('a'),b=required('b')){
    return a+b;
}

function required(m){
    throw Error(m + ' is required');
}

console.log(add(numberTwo, numberThree));



function* generateIterableSequence(){
    yield 'I';
    yield 'love';
    yield 'EPAM';
}

const generatorObject = generateIterableSequence();

for (let value of generatorObject) {
    console.log(value);
}