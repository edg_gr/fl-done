const numberTwo = 2;
const numberTen = 10;

function getBigestNumber(...num) {
    if (arguments.length<numberTwo){
        throw new Error('Not enough arguments')
    }
    if (arguments.length>numberTen){
        throw new Error('Too many arguments')
    }
    console.log(Math.max(...num))
    return Math.max(...num);
  }

module.exports = getBigestNumber;

getBigestNumber(numberTwo,numberTen);