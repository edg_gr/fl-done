const howLongToMyBirthday = require('../src/birthday.service');

describe('Working using a module', () => {
    it('should return number of days left to a birthday or days passed from it;', async () => {
      const daysToOrFromBirthday = howLongToMyBirthday;
      expect(daysToOrFromBirthday(`1211911992`)).toBe(1);
    });
})

/*
describe('test promise with jasmine', function(done) {
    var promise = getRejectedPromise();

    promise.then(function() {
      // Promise is resolved
      done(new Error('Promise should not be resolved'));
    }, function(reason) {
      // Promise is rejected
      // You could check rejection reason if you want to
      done(); // Success
    });
});
*/