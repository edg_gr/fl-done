const getBigestNumber = require('../src/get-bigest-number');
const numberFive = 5;
const numberTen = 10;

describe('Working using a module', () => {
    it('should return the biggest number;', () => {
      const biggestNumber = getBigestNumber;
      expect(biggestNumber(1, numberFive, numberTen)).toBe(numberTen);
    });
})